#!/usr/bin/env python2.7

from collections import defaultdict
import xml.etree.ElementTree
import locale
import codecs
import urllib2
import json
import sys
import re

# Wrap sys.stdout into a StreamWriter to allow writing unicode.
# FROM: http://stackoverflow.com/questions/4545661/unicodedecodeerror-when-redirecting-to-file
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

def fetch(pmid):

    sock = urllib2.urlopen(
            "http://www.ncbi.nlm.nih.gov/pubmed?term={0}&presentation=xml".format(
                    pmid, 
                )
            )

    xml_result = sock.read()
    useless_pre = xml.etree.ElementTree.fromstring(xml_result)

    xml_result = useless_pre.text.encode("utf-8")
    article = pubmed_xml_stripper(xml_result)

    sock.close()
    # Build BibTex and return

    unique_name = "{}{}".format(
                                    article.get("Author")[0].split(" ")[-1],
                                    str(article.get("Year")[0])[-2:]
                               )

    s = u"@article {" + unique_name + ",\n"
    if article.get("Author"):
        authors = article.get("Author")
        author_line = "\tAuthor={"
        i = 0
        while i < len(authors):
            if len(authors[i].split(" ")) == 2:
                author_line += ", ".join(reversed(authors[i].split(" ")))
            else:
                author_line += authors[i]
            if i != len(authors) - 1:
                author_line += " and "
            else:
                author_line += "},\n"
            i += 1
        s += author_line
        
    if article.get("Title"):
        title = article.get("Title")[0]
        
        s += u"\tTitle={" 
        s += re.sub(r"[A-Z]", lambda x: "{{{0}}}".format(x.group(0)), title)
        s += u"},\n"
    
    if article.get("Journal"):
        s += u"\tJournal={" + article.get("Journal")[0] + "},\n"

    if article.get("Year"):
        s += u"\tYear={" + article.get("Year")[0] + "},\n"

    if article.get("Volume"):
        s += u"\tVolume={" + article.get("Volume")[0] + "},\n"

    if article.get("Number"):
        s += u"\tNumber={" + article.get("Number")[0] + "},\n"

    if article.get("Pagination"):
        s += u"\tPages={" + article.get("Pagination")[0] + "},\n"

    # Enleve la virgule de trop (et le retour chariot).
    s = s[0:-2]
    s += u"\n}"
    print s


def pubmed_xml_stripper(s):
    root = xml.etree.ElementTree.fromstring(s)

    article_data = defaultdict(list)
    for element in root.iter():
        if element.tag == u"ArticleTitle":
            # The article title
            article_data[u"Title"].append(element.text)

        elif element.tag == u"Author":
            # Author information
            collective = element.find("CollectiveName")
            if 'text' in dir(collective):
                article_data[element.tag].append(u"{}".format(collective.text))
            else:
                lname = element.find("LastName").text
                initials = element.find("Initials").text
                article_data[element.tag].append(u"{}. {}".format(initials, lname))

        elif element.tag == u"Journal":
            # Journal information
            article_data[u"Journal"].append(element.find(u"ISOAbbreviation").text)
            issue = element.find(u"JournalIssue")
            if issue.find(u"PubDate") is not None:
                article_data[u"Year"].append(issue.find(u"PubDate").find(u"Year").text)

            if issue.find(u"Volume") is not None:
                article_data[u"Volume"].append(issue.find(u"Volume").text)

            if issue.find(u"Issue") is not None:
                article_data[u"Issue"].append(issue.find(u"Issue").text)

        elif element.tag == u"MedlinePgn":
            # The pagination
            if element.text:
                article_data[u"Pagination"].append(element.text)

        elif element.tag == u"ArticleId":
            if element.get(u"IdType") == u"doi":
                article_data[u"DOI"].append(element.text)

    return article_data

def print_usage():
    # TODO
    print "Usage"
    exit(0)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print_usage()
    else:
        fetch(sys.argv[1])
